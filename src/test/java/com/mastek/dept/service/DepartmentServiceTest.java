package com.mastek.dept.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.junit.Test;

import com.mastek.dept.domain.Department;

public class DepartmentServiceTest {

	@Test	
	public void departmentServiceCrudTest() {
		DepartmentService service = new DepartmentService();
		List<Department> existingDepartments = service.getAllDepartments();
		assertFalse(existingDepartments.isEmpty());
		Optional<Long> maxId = existingDepartments.stream().map(d -> d.getId()).max(Comparator.naturalOrder());
		assertTrue(maxId.isPresent());
		Department department = new Department(maxId.get() + 1, "Dept", "Dept Desc");
		int previousSize = service.getAllDepartments().size();
		service.addDepartment(department);
		assertEquals(previousSize + 1, service.getAllDepartments().size());

		Department departmentById = service.getDepartmentById(maxId.get() + 1);
		assertTrue(department == departmentById);

		final String desc = "Updated Deptartment";
		departmentById.setDesc(desc);
		Department updateDepartment = service.updateDepartment(departmentById.getId(), departmentById);
		assertEquals(desc, updateDepartment.getDesc());

		service.removeDepartment(maxId.get() + 1);
		assertEquals(previousSize, service.getAllDepartments().size());
	}

}
