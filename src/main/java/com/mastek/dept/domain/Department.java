package com.mastek.dept.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat
public class Department {

	private long id;
	private String name;
	private Date created;
	private String desc;

	public Department() {
	}

	public Department(long id, String name, String desc) {
		this.id = id;
		this.name = name;
		this.desc = desc;
		this.setCreated(new Date());
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Override
	public String toString() {
		return "Department [id=" + id + ", name=" + name + ", created=" + created + ", desc=" + desc + "]";
	}

}
