package com.mastek.dept.exceptions;

public class InvalidInputException extends RuntimeException {

	private static final long serialVersionUID = 5848995327043904938L;

	public InvalidInputException() {
		super("Invalid Input");
	}

	public InvalidInputException(String message) {
		super(message);
	}
}