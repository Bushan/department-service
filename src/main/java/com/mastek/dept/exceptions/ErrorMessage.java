package com.mastek.dept.exceptions;

public class ErrorMessage {

	private String reason;

	public ErrorMessage() {
		// TODO Auto-generated constructor stub
	}

	public ErrorMessage(String msg) {
		this.reason = msg;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

}
