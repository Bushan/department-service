package com.mastek.dept.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mastek.dept.domain.Department;
import com.mastek.dept.service.DepartmentService;

@RestController
@RequestMapping(value = "/departments")
public class DepartmentController {

	@Autowired
	DepartmentService departmentService;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Department>> getDepartments() {
		return new ResponseEntity<List<Department>>(departmentService.getAllDepartments(), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json", path = "/{id}")
	public ResponseEntity<Department> getDepartmentById(@PathVariable("id") long id) {
		return new ResponseEntity<Department>(departmentService.getDepartmentById(id), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Department> addDepartment(@RequestBody Department department) {
		return new ResponseEntity<Department>(departmentService.addDepartment(department), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json", path = "/{id}")
	public ResponseEntity<Department> updateDepartment(@PathVariable("id") long id,
			@RequestBody Department department) {
		return new ResponseEntity<Department>(departmentService.updateDepartment(id, department), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json", path = "/{id}")
	public void deleteDepartment(@PathVariable("id") long id) {
		departmentService.removeDepartment(id);
	}

}
