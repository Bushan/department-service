package com.mastek.dept.repository;

import java.util.HashMap;
import java.util.Map;

import com.mastek.dept.domain.Department;

public class DepartmentRepo {

	private static Map<Long, Department> departments = new HashMap<>();

	public static Map<Long, Department> getDepartments() {
		return departments;
	}

}
