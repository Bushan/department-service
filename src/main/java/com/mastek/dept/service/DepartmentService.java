package com.mastek.dept.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.mastek.dept.domain.Department;
import com.mastek.dept.repository.DepartmentRepo;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Service
public class DepartmentService {

	private Map<Long, Department> departments = DepartmentRepo.getDepartments();

	private volatile int counter = 0;

	public DepartmentService() {
		// Initiating with default employees for testing
		departments.put(1L, new Department(1, "Accounts", "Account department"));
		departments.put(2L, new Department(2, "Shipping", "Shipping department"));
	}

	@HystrixCommand(fallbackMethod = "fallbackDepartments", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000"),
			@HystrixProperty(name = "execution.isolation.strategy", value = "SEMAPHORE") })
	public List<Department> getAllDepartments() {
		this.counter++;
		System.out.println("Current counter size from allDepartments ..." + this.counter);
		if (this.counter % 5 == 0) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new ArrayList<Department>(departments.values());
	}

	@HystrixCommand(fallbackMethod = "fallbackDepartmentById", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "3000"),
			@HystrixProperty(name = "execution.isolation.strategy", value = "SEMAPHORE") })
	public Department getDepartmentById(long id) {
		this.counter++;
		System.out.println("Current counter size from departmentById ..." + this.counter);
		if (this.counter % 5 == 0) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Department department = departments.get(id);
		if (department == null) {
			throw new RuntimeException("No department exist with provided id");
		}
		return departments.get(id);
	}

	public Department updateDepartment(long id, Department department) {
		Department departmentToUpdate = departments.get(id);
		if (departmentToUpdate == null) {
			return null;
		}
		departmentToUpdate.setName(department.getName());
		departmentToUpdate.setDesc(department.getDesc());
		departments.put(id, departmentToUpdate);
		return departmentToUpdate;
	}

	public Department addDepartment(Department department) {
		department.setId(departments.size() + 1L);
		department.setCreated(new Date());
		departments.put(department.getId(), department);
		return department;
	}

	public Department removeDepartment(long id) {
		return departments.remove(id);
	}

	public Department fallbackDepartmentById(long id) {
		return new Department(departments.size() + 1, "Default", "Default department");
	}

	public List<Department> fallbackDepartments() {
		return Arrays.asList(new Department(departments.size() + 1, "Default", "Default department"));
	}

}
