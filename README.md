# Department Service

This is sample service for departments.

## Running

Running from CLI
 
	$ java -jar department-service-0.0.1.jar

or, Running main method class 'DepartmentServiceApplication.java' from IDE or CLI

Its configured to run on port '8096' 

This get auto register with Eureka Discovery service to serve the Eureka API from "/eureka".

## Resources

	POST http://${host}:8096/departments/
 	{
    	"name": "Accounts",
    	"desc": "Account department"
  	}

	GET http://${host}:8096/departments/
	GET http://${host}:8096/departments/{department_Id}

	PUT http://${host}:8096/departments/{department_Id}	
	{

    "name": "Accounts",   
    "desc": "Accounts department updated"
  	}
  
	DELETE http://${host}:8096/departments/{department_Id}
	
## Hystrix
To monitor hystrix stream service circuiteBreaker is enabled and can be monitor at URL, use same url to monitor on Hystrix Dashboard
	
	http://${host}:8096/hystrix.stream

## Docker

Running in Docker container

	$ docker run -p 8096:8096 -e "EUREKA_SERVER_HOST=<server IP>" -e "EUREKA_SERVER_PORT=<server port e.g. 8761>" -t <image-prefix>/department-service:<image-tag>


Its configured to build docker image and push to private nexus repository using maven plugin.
	
	mvn clean -X package docker:build -DpushImage

Maven properties

	docker.image.prefix=<nexus host>:18079
	docker.registry.id=<Docker Repository name>
	registry.url=<registry url e.g. https://192.168.1.98:8449/repository/myDockerRepo/ >
	
